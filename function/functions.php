<?php
$nav = ["home", "about", "project", "skill", "contact"];

$project = [["image" => "project.png", "title" => "project", "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, praesentium? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut adipisci minima reprehenderit facere impedit dignissimos odit debitis sed fugiat provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut adipisci minima reprehenderit facere impedit dignissimos odit debitis sed fugiat provident. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, praesentium? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut adipisci minima reprehenderit facere impedit dignissimos odit debitis sed fugiat provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut adipisci minima reprehenderit facere impedit dignissimos odit debitis sed fugiat provident. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, praesentium? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut adipisci minima reprehenderit facere impedit dignissimos odit debitis sed fugiat provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut adipisci minima reprehenderit facere impedit dignissimos odit debitis sed fugiat provident. Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, praesentium? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut adipisci minima reprehenderit facere impedit dignissimos odit debitis sed fugiat provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut adipisci minima reprehenderit facere impedit dignissimos odit debitis sed fugiat provident."], ["image" => "project.png", "title" => "kerjaan", "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, praesentium?"], ["image" => "project.png", "title" => "bekerja", "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, praesentium?"], ["image" => "project.png", "title" => "pekerja", "desc" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, praesentium?"]];
$skill = [["image" => "Web-Design.svg", "alt" => "Web-Design", "skill" => "Web Design"], ["image" => "3D-Model.svg", "alt" => "3D-Model", "skill" => "3d Modeling"], ["image" => "Photo-Product.svg", "alt" => "Photo-Product", "skill" => "Photo Product"], ["image" => "Motion-Graphic.svg", "alt" => "Motion-Graphic", "skill" => "Motion Graphic"]];

$social = [["link" => "https://www.instagram.com/", "name" => "instagram"], ["link" => "https://facebook.com/", "name" => "facebook"], ["link" => "https://www.youtube.com/", "name" => "youtube"], ["link" => "https://twitter.com/", "name" => "twitter"], ["link" => "https://gitlab.com/", "name" => "gitlab"], ["link" => "https://github.com/", "name" => "github"], ["link" => "https://bitbucket.com/", "name" => "bitbucket"], ["link" => "https://codepen.io/", "name" => "codepen"], ["link" => "https://freecodecamp.org/", "name" => "freecodecamp"], ["link" => "https://sololearn.com/", "name" => "sololearn"], ["link" => "https://coursera.org/", "name" => "coursera"], ["link" => "https://codewars.com/", "name" => "codewars"], ["link" => "https://linkedin.com/", "name" => "linkedin"]];

$eEmail = false;
$eName = false;
$eMessage = false;
function sendmail($data)
{
	if (!empty($data["email"])) {
		$fromEmail = htmlspecialchars($data["email"]);
		$fromEmail = "From: " . $fromEmail;
	} else {
		return $eEmail = true;
	}
	$toEmail = "emailsaya@gmail.com";
	if (!empty($data["name"])) {
		$subject = htmlspecialchars($data["name"]);
		$subject = "From: " . $subject;
	} else {
		return $eName = true;
	}
	if (!empty($data["message"])) {
		$message = htmlspecialchars($data["message"]);
	} else {
		return $eMessage = true;
	}

	return mail($toEmail, $subject, $message, $fromEmail);
}