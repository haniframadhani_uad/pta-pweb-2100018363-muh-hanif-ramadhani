<?php
require 'function/functions.php';
if (isset($_POST["submit"])) {
	if (!empty($_POST["name"]) && !empty($_POST["email"] && !empty($_POST["message"]))) {
		if (sendmail($_POST) == true) {
			echo "<script>
        	alert('terima kasih telah menghubungi saya');
        	</script>";
		} else {
			echo "<script>
        	alert('email gagal terkirim');
        	</script>";
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Projek Tugas Akhir</title>
</head>

<body id="home">
    <nav class="navbar sticky offset">
        <div class="container nav-mobile">
            <div class="logo">
                <h1><a href="#">hanif</a></h1>
            </div>

            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>

            <ul class="nav-links">
                <?php foreach ($nav as $n) : ?>
                <li class="nav-item"><a href="#<?= $n ?>" class="alink" data-target="<?= $n ?>"><?= $n ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </nav>

    <div class="hero">
        <img src="img/profile.jpg" alt="profile">
        <h1>muhammad hanif ramadhani</h1>
        <h3>2100018363</h3>
    </div>

    <main>
        <div class="article">
            <div class="container">
                <div class="main-content">
                    <div class="about" id="about">
                        <h1>about</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A facere, iure nisi quos voluptas
                            illum eius
                            odio,
                            vero necessitatibus architecto at animi deleniti, iste alias. Voluptas aliquam expedita ad
                            vero nisi
                            facere,
                            eaque qui magnam aperiam beatae officia porro in minima totam tenetur laudantium odio
                            ratione natus illum
                            perferendis sit!</p>
                    </div>
                    <div class="project" id="project">
                        <h1>project</h1>
                        <div class="card-list">
                            <?php foreach ($project as $p) : ?>
                            <div class="card">
                                <div class="image">
                                    <img src="img/<?= $p["image"] ?>" alt="project">
                                </div>
                                <div class="content">
                                    <h3><?= $p["title"] ?></h3>
                                    <p><?= $p["desc"] ?></p>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <div class="skill" id="skill">
                        <h1>skill</h1>
                        <div class="card-list">
                            <?php foreach ($skill as $s) : ?>
                            <div class="skill-card">
                                <div class="image">
                                    <img src="img/<?= $s["image"] ?>" alt="<?= $s["alt"] ?>">
                                </div>
                                <div class="content">
                                    <p><?= $s["skill"] ?></p>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <aside class="sidebar">
            <div class="container social">
                <ul>
                    <?php foreach ($social as $s) : ?>
                    <li><a href="<?= $s["link"] ?>"><?= $s["name"] ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </aside>
    </main>

    <footer id="contact">
        <div class="container">
            <h1>contact me</h1>
            <form action="" method="post">
                <label for="name">name : </label><br>
                <input type="text" name="name" id="name" placeholder="enter your name" autocomplete="off"><br>
                <div class="error-name">
                    <p>please enter your name</p>
                </div>
                <label for="email">email : </label><br>
                <input type="text" name="email" id="email" placeholder="enter your email" autocomplete="off"><br>
                <div class="error-email">
                    <p>please enter your email</p>
                </div>
                <label for="message">message : </label><br>
                <textarea name="message" id="message" placeholder="enter your message"></textarea>
                <div class="error-message">
                    <p>please entar the message</p>
                </div>
                <button type="submit" name="submit" class="btn">mail me!</button>
            </form>
        </div>
    </footer>

    <script src="script.js"></script>
</body>

</html>