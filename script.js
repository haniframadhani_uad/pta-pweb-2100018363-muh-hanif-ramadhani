const hamburger = document.querySelector('.hamburger');
const navLinks = document.querySelector('.nav-links');
const btn = document.querySelector('.btn');
hamburger.addEventListener('click', () => {
    hamburger.classList.toggle('active');
    navLinks.classList.toggle('active');
});

const ename = document.querySelector('.error-name');
const eemail = document.querySelector('.error-email');
const emessage = document.querySelector('.error-message');
const inputName = document.getElementById('name');
const inputEmail = document.getElementById('email');
const inputMessage = document.getElementById('message');

btn.addEventListener("click", e => {
    if (inputName.value == '') {
        ename.classList.add("error");
        e.preventDefault();
    } else {
        ename.classList.remove("error");
    }
    if (inputEmail.value == '') {
        eemail.classList.add("error");
        e.preventDefault();
    } else {
        eemail.classList.remove("error");
    }
    if (inputMessage.value == '') {
        emessage.classList.add("error");
        e.preventDefault();
    } else {
        emessage.classList.remove("error");
    }
});

